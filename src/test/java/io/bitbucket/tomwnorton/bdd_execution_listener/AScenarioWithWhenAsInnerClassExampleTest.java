/*
 * bdd-extension-listener
 * Copyright (C) 2018  Tom Norton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.bitbucket.tomwnorton.bdd_execution_listener;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Given a test class")
class AScenarioWithWhenAsInnerClassExampleTest {
    @DisplayName("When something happens")
    @Nested
    class Inner_class {
        @DisplayName("then something else should happen")
        @Test
        void the_test() {
        }
    }
}
