/*
 * bdd-extension-listener
 * Copyright (C) 2018  Tom Norton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.bitbucket.tomwnorton.bdd_execution_listener;

import org.assertj.core.api.Condition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.platform.engine.discovery.DiscoverySelectors.selectClass;

@DisplayName("Given a BDDExecutionListener")
class BDDExecutionListenerTest implements Scenario.Recorder {
    private BDDExecutionListener listener;
    private StringBuilder scenarioSummary;
    private List<Scenario> actualScenarios;

    @BeforeEach
    void setUp() {
        listener = new BDDExecutionListener() {
            @Override
            protected Scenario.Recorder createScenarioRecorder() {
                return BDDExecutionListenerTest.this;
            }
        };
        scenarioSummary = new StringBuilder();
        actualScenarios = new ArrayList<>();
    }

    @DisplayName("when a test whose display name doesn't start with 'Given' is executed then there are no scenarios")
    @Test
    void it_records_no_scenarios_for_test_class_without_any_givens() {
        runTestsInClass(ANormalTestExampleTest.class);
        assertScenarios();
        assertSummary("");
    }

    @DisplayName("it records one properly constructed scenario when a test class starting with 'Given' and a test method in the format of 'when ... then ...' is executed")
    @Test
    void it_can_handle_a_simple_scenario_with_when_then_test() {
        runTestsInClass(ASimpleGivenWhenThenWithWhenThenTestExampleTest.class);
        assertScenarios(scenario()
                                .given("the system is in some state")
                                .when("some action occurs")
                                .then("the system is in some other state")
                                .build());
        assertSummary("(GWT)");
    }

    @DisplayName("it records one properly constructed scenario when a test class starting with 'Given' and a test method in the format of 'it ... when ...' is executed")
    @Test
    void it_can_handle_a_simple_scenario_with_it_when_test() {
        runTestsInClass(ASimpleGivenWhenThenWithItWhenTestExampleTest.class);
        assertScenarios(scenario()
                                .given("a test class")
                                .when("some event takes place")
                                .then("does something cool")
                                .build());
        assertSummary("(GWT)");
    }

    @DisplayName("it records two properly constructed scenarios when a 'Given' test class containing multiple tests is executed")
    @Test
    void it_can_handle_a_given_class_with_multiple_scenarios_as_different_tests() {
        runTestsInClass(AGivenWithTwoScenariosAsSeparateTestsExampleTest.class);
        assertScenarios(
                scenario()
                        .given("a test class")
                        .when("the first action is executed")
                        .then("the first assert is performed")
                        .build(),
                scenario()
                        .given("a test class")
                        .when("the second action is executed")
                        .then("performs the second assert")
                        .build()
        );
        assertSummary("(GWT GWT)");
    }

    @DisplayName("it parses the classes and tests when different casing is used for the keywords")
    @Test
    void it_uses_case_insensitive_parsing_for_the_keywords() {
        runTestsInClass(ATestClassWithCaseInsensitiveKeywordsExampleTest.class);
        assertScenarios(
                scenario()
                        .given("a test class")
                        .when("something happens")
                        .then("assert the state")
                        .build(),
                scenario()
                        .given("a test class")
                        .when("something happens")
                        .then("asserts the state")
                        .build()
        );
        assertSummary("(GWT GWT)");
    }

    @DisplayName("it creates the proper scenario when a class with nested givens is executed")
    @Test
    void it_can_handle_a_scenario_with_multiple_givens() {
        runTestsInClass(AScenarioWithMultipleGivensExampleTest.class);
        assertScenarios(scenario()
                                .given("an outer test class")
                                .given("a nested test class")
                                .given("another nested test class")
                                .when("something happens")
                                .then("assert something else")
                                .build());
        assertSummary("(GGGWT)");
    }

    @DisplayName("it creates the proper scenario when a nested class is used as 'When' instead of a test method")
    @Test
    void it_can_handle_when_as_a_class_instead_of_a_method() {
        runTestsInClass(AScenarioWithWhenAsInnerClassExampleTest.class);
        assertScenarios(scenario()
                                .given("a test class")
                                .when("something happens")
                                .then("something else should happen")
                                .build());
        assertSummary("(GWT)");
    }

    @DisplayName("it creates the proper scenarios when a 'When' test class contains multiple 'Then' tests")
    @Test
    void it_can_handle_a_when_class_with_multiple_then_tests() {
        runTestsInClass(AScenarioWithMultipleThensPerWhenExampleTest.class);
        assertScenarios(scenario()
                                .given("a test class")
                                .when("something happens")
                                .then("assert the first condition")
                                .then("assert the second condition")
                                .build());
        assertSummary("(GWTT)");
    }

    @DisplayName("it creates the proper scenario when a class with nested whens is executed")
    @Test
    void it_can_handle_a_scenario_with_multiple_whens() {
        runTestsInClass(AScenarioWithNestedWhensExampleTest.class);
        assertScenarios(scenario()
                                .given("a test class")
                                .when("something happens")
                                .when("something else happens")
                                .then("assert something cool happened")
                                .build());
        assertSummary("(GWWT)");
    }

    @DisplayName("it creates the proper scenario when a class has a failing when...then... test")
    @Test
    void it_can_handle_a_failing_when_then_test() {
        Throwable expectedException = AScenarioWithAFailingWhenThenTestExampleTest.EXCEPTION_TO_THROW;
        runTestsInClass(AScenarioWithAFailingWhenThenTestExampleTest.class);
        assertScenarios(scenario()
                                .given("a test class")
                                .when("something happens")
                                .then("do something else", Scenario.Step.Result.FAILURE, expectedException)
                                .build());
    }

    private void runTestsInClass(Class<?> testClass) {
        LauncherDiscoveryRequest request =
                LauncherDiscoveryRequestBuilder.request()
                                               .selectors(selectClass(testClass))
                                               .build();
        Launcher launcher = LauncherFactory.create();
        launcher.registerTestExecutionListeners(listener);
        launcher.execute(request);
    }

    @Override
    public void start() {
        scenarioSummary.append("(");
    }

    @Override
    public void record(Scenario scenario) {
        if (!"(".equals(scenarioSummary.toString())) {
            scenarioSummary.append(" ");
        }
        for (String ignored : scenario.getGivens()) {
            scenarioSummary.append("G");
        }
        for (String ignored : scenario.getWhens()) {
            scenarioSummary.append("W");
        }
        for (Scenario.Step ignored : scenario.getThens()) {
            scenarioSummary.append("T");
        }
        actualScenarios.add(scenario);
    }

    @Override
    public void finish() {
        scenarioSummary.append(")");
    }

    @SafeVarargs
    private final void assertScenarios(Condition<Scenario>... conditions) {
        for (Condition<Scenario> condition : conditions) {
            assertThat(actualScenarios).haveExactly(1, condition);
        }
    }

    private void assertSummary(String expectedSummary) {
        assertThat(scenarioSummary.toString()).isEqualTo(expectedSummary);
    }

    private static ScenarioConditionBuilder scenario() {
        return new ScenarioConditionBuilder();
    }

    private static class ScenarioConditionBuilder {
        private final List<String> expectedGivens = new ArrayList<>();
        private final List<String> expectedWhens = new ArrayList<>();
        private final List<Scenario.Step> expectedThens = new ArrayList<>();

        ScenarioConditionBuilder given(String value) {
            expectedGivens.add(value);
            return this;
        }

        ScenarioConditionBuilder when(String value) {
            expectedWhens.add(value);
            return this;
        }

        ScenarioConditionBuilder then(String value) {
            expectedThens.add(new Scenario.Step(value, Scenario.Step.Result.SUCCESS, null));
            return this;
        }

        ScenarioConditionBuilder then(String value, Scenario.Step.Result result, Throwable expectedException) {
            expectedThens.add(new Scenario.Step(value, result, expectedException));
            return this;
        }

        Condition<Scenario> build() {
            StringBuilder description = new StringBuilder();
            for (String given : expectedGivens) {
                description.append("GIVEN ").append(given).append("\n");
            }
            for (String when : expectedWhens) {
                description.append("WHEN ").append(when).append("\n");
            }
            for (Scenario.Step then : expectedThens) {
                description.append("THEN ").append(then).append("\n");
            }
            return new Condition<Scenario>(description.toString()) {
                @Override
                public boolean matches(Scenario value) {
                    if (value == null) {
                        return false;
                    }
                    List<Scenario.Step> actualThens = convertIterableToList(value.getThens());
                    return expectedGivens.equals(convertIterableToList(value.getGivens()))
                            && expectedWhens.equals(convertIterableToList(value.getWhens()))
                            // We can't expect tests to be called in the same order every time
                            && expectedThens.size() == actualThens.size()
                            && actualThens.containsAll(expectedThens);
                }
            };
        }
    }

    private static <T> List<T> convertIterableToList(Iterable<T> iterable) {
        return StreamSupport.stream(iterable.spliterator(), false)
                            .collect(Collectors.toList());
    }
}