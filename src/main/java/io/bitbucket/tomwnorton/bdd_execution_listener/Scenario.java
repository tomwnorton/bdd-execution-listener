/*
 * bdd-extension-listener
 * Copyright (C) 2018  Tom Norton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.bitbucket.tomwnorton.bdd_execution_listener;

import java.util.*;

/**
 * Encapsulates a given...when...then scenario
 */
@SuppressWarnings("WeakerAccess")
public class Scenario {
    private Deque<String> givens = new ArrayDeque<>();
    private Deque<String> whens = new ArrayDeque<>();
    private Collection<Step> thens = new ArrayList<>();

    /**
     * {@code then}s should be added last-to-first
     */
    public void addGiven(String given) {
        givens.addFirst(given);
    }

    public Iterable<String> getGivens() {
        return givens;
    }

    /**
     * {@code when}s should be added last-to-first
     */
    public void addWhen(String when) {
        whens.addFirst(when);
    }

    public Iterable<String> getWhens() {
        return whens;
    }

    /**
     * Unlike {@code given}s or {@code when}s, {@code then}s are added first-to-last
     *
     * @param then      The description for the {@code then} step
     * @param result    Whether the {@code then} succeeded or failed
     * @param throwable The exception that caused the failure, or {@code null}
     */
    public void addThen(String then, Step.Result result, Throwable throwable) {
        thens.add(new Step(then, result, throwable));
    }

    public Iterable<Step> getThens() {
        return thens;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (String given : givens) {
            result.append("GIVEN ").append(given).append("\n");
        }
        for (String when : whens) {
            result.append("WHEN ").append(when).append("\n");
        }
        for (Step then : thens) {
            result.append("THEN ").append(then).append("\n");
        }
        return result.toString();
    }

    public static class Step {
        public enum Result {
            SUCCESS,
            FAILURE
        }

        private String value;
        private Result result;
        private Throwable throwable;

        public Step(String value, Result result, Throwable throwable) {
            if (value == null || value.trim().equals("")) {
                throw new IllegalArgumentException("value must not be null or empty");
            }
            if (result == null) {
                throw new IllegalArgumentException("result must not be null");
            }
            this.value = value;
            this.result = result;
            this.throwable = throwable;
        }

        public String getValue() {
            return value;
        }

        public Result getResult() {
            return result;
        }

        public void setResult(Result result) {
            if (result == null) {
                throw new IllegalArgumentException("result must not be null");
            }
            this.result = result;
        }

        public Optional<Throwable> getThrowable() {
            return Optional.ofNullable(throwable);
        }

        public void setThrowable(Throwable throwable) {
            this.throwable = throwable;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Step step = (Step) o;
            return Objects.equals(value, step.value) &&
                    result == step.result &&
                    Objects.equals(throwable, step.throwable);
        }

        @Override
        public int hashCode() {
            return Objects.hash(value, result, throwable);
        }

        @Override
        public String toString() {
            return value + " (" + result + ")";
        }
    }

    /**
     * Plugin to serialize the {@link Scenario}s in the test plan
     * <p>
     * All plugins are loaded using Java's {@link ServiceLoader}.  So, you're jar's @{code META-INF/services} directory should
     * contain a file called {@code io.bitbucket.tomwnorton.bdd_execution_listener.Scenario$Recorder}.  See the JDK documentation
     * for more details on the {@link ServiceLoader}
     */
    public interface Recorder {
        /**
         * Called before the first {@link Scenario} in the test plan is recorded. Will never run if there are no {@link Scenario}s
         */
        default void start() {
        }

        /**
         * Called for each {@link Scenario} in the test plan
         */
        void record(Scenario scenario);

        /**
         * Called after the last {@link Scenario} in the test plan is recorded. Will never run if there are no {@link Scenario}s
         */
        default void finish() {
        }
    }
}
