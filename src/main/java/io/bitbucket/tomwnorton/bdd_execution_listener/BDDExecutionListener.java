/*
 * bdd-extension-listener
 * Copyright (C) 2018  Tom Norton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.bitbucket.tomwnorton.bdd_execution_listener;

import org.junit.platform.engine.TestExecutionResult;
import org.junit.platform.launcher.TestExecutionListener;
import org.junit.platform.launcher.TestIdentifier;
import org.junit.platform.launcher.TestPlan;

import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.function.BiConsumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BDDExecutionListener implements TestExecutionListener {
    private Map<String, TestContainerModel> testContainerModelMap = new HashMap<>();
    private Map<String, Scenario> uniqueWhenIdToScenarioMap = new HashMap<>();
    private int numberOfScenariosProcessed;
    private Scenario.Recorder scenarioRecorder = createScenarioRecorder();

    @Override
    public void testPlanExecutionFinished(TestPlan testPlan) {
        for (Scenario scenario : uniqueWhenIdToScenarioMap.values()) {
            recordScenario(scenario);
        }
        uniqueWhenIdToScenarioMap.clear();
        if (numberOfScenariosProcessed > 0) {
            scenarioRecorder.finish();
        }
    }

    @Override
    public void executionStarted(TestIdentifier testIdentifier) {
        String displayName = testIdentifier.getDisplayName();
        Matcher givenMatcher = createCaseInsensitiveMatcher("^Given (?<given>.*)$", displayName);
        if (givenMatcher.find()) {
            testContainerModelMap.put(testIdentifier.getUniqueId(),
                                      new TestContainerModel(testIdentifier.getUniqueId(),
                                                             testIdentifier.getParentId().orElse(null),
                                                             givenMatcher.group("given"),
                                                             TestContainerModel.Type.GIVEN));
        } else {
            Matcher whenMatcher = createCaseInsensitiveMatcher("^When (?<when>.*)$", displayName);
            if (whenMatcher.find()) {
                testContainerModelMap.put(testIdentifier.getUniqueId(),
                                          new TestContainerModel(testIdentifier.getUniqueId(),
                                                                 testIdentifier.getParentId().orElse(null),
                                                                 whenMatcher.group("when"),
                                                                 TestContainerModel.Type.WHEN));
            }
        }
    }

    @Override
    public void executionFinished(TestIdentifier testIdentifier, TestExecutionResult testExecutionResult) {
        if (!testIdentifier.isTest()
                || !testIdentifier.getParentId().isPresent()
                || !testContainerModelMap.containsKey(testIdentifier.getParentId().get())) {
            return;
        }

        String parentId = testIdentifier.getParentId().get();
        String displayName = testIdentifier.getDisplayName();
        Scenario scenario = createScenario(parentId, displayName, testExecutionResult);

        if (scenario != null && !uniqueWhenIdToScenarioMap.containsKey(parentId)) {
            recordScenario(scenario);
        }
    }

    private void recordScenario(Scenario scenario) {
        if (numberOfScenariosProcessed++ == 0) {
            scenarioRecorder.start();
        }
        scenarioRecorder.record(scenario);
    }

    private Scenario createScenario(String parentId, String displayName, TestExecutionResult testExecutionResult) {
        Matcher whenThenMatcher = createCaseInsensitiveMatcher("^when (?<when>.*) then (?<then>.*)$", displayName);
        Matcher itWhenMatcher = createCaseInsensitiveMatcher("^it (?<then>.*) when (?<when>.*)$", displayName);
        Matcher thenOnlyMatcher = createCaseInsensitiveMatcher("^then (?<then>.*)$", displayName);
        if (whenThenMatcher.find()) {
            return createScenarioFromParentIdAndRegex(parentId, whenThenMatcher, testExecutionResult);
        }
        if (itWhenMatcher.find()) {
            return createScenarioFromParentIdAndRegex(parentId, itWhenMatcher, testExecutionResult);
        }
        if (thenOnlyMatcher.find()) {
            return createScenarioFromParentIdAndRegex(parentId, thenOnlyMatcher, testExecutionResult);
        }
        return null;
    }

    private Scenario createScenarioFromParentIdAndRegex(String directParentId,
                                                        Matcher matcher,
                                                        TestExecutionResult testExecutionResult) {
        Scenario scenario = findCachedScenario(directParentId);
        try {
            String when = matcher.group("when");
            if (when != null) {
                scenario.addWhen(when);
            }
        } catch (IllegalArgumentException ignored) {
        }
        scenario.addThen(matcher.group("then"),
                         convertJUnitStatusIntoBDDResult(testExecutionResult.getStatus()),
                         testExecutionResult.getThrowable().orElse(null));
        return scenario;
    }

    private Scenario.Step.Result convertJUnitStatusIntoBDDResult(TestExecutionResult.Status status) {
        switch (status) {
            case FAILED:
                return Scenario.Step.Result.FAILURE;
            default:
                return Scenario.Step.Result.SUCCESS;
        }
    }

    private Matcher createCaseInsensitiveMatcher(String regex, String text) {
        return Pattern.compile(regex, Pattern.CASE_INSENSITIVE).matcher(text);
    }

    private Scenario findCachedScenario(String directParentId) {
        Scenario scenario = new Scenario();
        String parentId = directParentId;
        do {
            TestContainerModel testContainerModel = testContainerModelMap.get(parentId);
            testContainerModel.populate(scenario);
            parentId = testContainerModel.parentId;
        } while (parentId != null && testContainerModelMap.containsKey(parentId));
        TestContainerModel testContainerModel = testContainerModelMap.get(directParentId);
        if (testContainerModel != null && testContainerModel.type == TestContainerModel.Type.WHEN) {
            return uniqueWhenIdToScenarioMap.computeIfAbsent(directParentId, key -> scenario);
        }
        return scenario;
    }

    protected Scenario.Recorder createScenarioRecorder() {
        ServiceLoader<Scenario.Recorder> scenarioRecorderLoader = ServiceLoader.load(Scenario.Recorder.class);

        return new Scenario.Recorder() {
            @Override
            public void start() {
                for (Scenario.Recorder recorder : scenarioRecorderLoader) {
                    recorder.start();
                }
            }

            @Override
            public void record(Scenario scenario) {
                for (Scenario.Recorder recorder : scenarioRecorderLoader) {
                    recorder.record(scenario);
                }
            }

            @Override
            public void finish() {
                for (Scenario.Recorder recorder : scenarioRecorderLoader) {
                    recorder.finish();
                }
            }
        };
    }

    private static class TestContainerModel {
        final String id;
        final String parentId;
        final String displayName;
        final Type type;

        enum Type {
            GIVEN(Scenario::addGiven),
            WHEN(Scenario::addWhen);

            private final BiConsumer<Scenario, String> consumer;

            Type(BiConsumer<Scenario, String> consumer) {
                this.consumer = consumer;
            }
        }

        TestContainerModel(String id,
                           String parentId,
                           String displayName,
                           Type type) {
            this.id = id;
            this.parentId = parentId;
            this.displayName = displayName;
            this.type = type;
        }

        void populate(Scenario scenario) {
            type.consumer.accept(scenario, displayName);
        }
    }
}
