# BDD Execution Listener
This is a JUnit5 plugin that encapsulates your tests into BDD-style
scenario models.  It has a plugin-based architecture for serializing these
models.  See
[the documentation](https://tomwnorton.bitbucket.io/jvm/bdd-execution-listener)
for details.